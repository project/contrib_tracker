The Drupal 8 Contrib Porting Tracker provides a centralized place for tracking
the porting status of contributed projects (modules, themes, distributions).
This project consists of issues that show the D8 porting status of other
projects, and since it is only an issue queue, the code repository is unused.
For information, see the project page:
https://www.drupal.org/project/contrib_tracker
